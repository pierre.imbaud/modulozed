===========
 modulozed
===========


This is a python project, attempting to master rings created on modulo
operations.

ℤ is a ring. for any integer a, a*ℤ is a subring, comprising all the
multiples of a. ℤ / a*ℤ is the quotient of ℤ by a*ℤ, it has a
elements.

These are finite sets, and rings as well, we can add and multiply
elements.

The project is rather naive, chances are mathematical modules
adressing this already exist. If so, let me know, and consider this as
a personnal exercise.

We shall use ℤa, rather than ℤ / a*ℤ.

And in code classes ℤa will be named Za, keeping ascii name for identifiers.

