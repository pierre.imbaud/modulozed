========
 Fields
========

(en français: corps)
The statement we would like to proove here is: ℤ / p*ℤ is a field if
and only if p is prime.

Only if:
========

a primary, quite obvious result is that ℤ / a*ℤ is not a field if a is
not prime.

if a is not prime, it has at least two divisors, b and c. in ℤ / a*ℤ,
b and c become divisors of zero. A ring with divisors of zero cannot
be a field, qed.

proof on prime p
================

To proove ℤ / p*ℤ is a field, we need to proove for any non null number a < p,
exists b non null < p verifying a*p modulo p == 1.

We shall iterate on couples (a, b) in [1.. p-1] squared, building for
each (a * b) ~ p, and keeping couples for which the result
equals 1. These might be kept in a dict with a as key, b as value, and
vice versa of course. Proof is reached when all a in [1.. p-1] are keys.

