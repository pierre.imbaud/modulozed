This dir contains modules **and** text about demos:

- Statements we try to proof.
- text yielding a mathematical proof.
- modules providing an experimental proof, more ofthen than not up to
  a point.
- eventually, references to proofs provided by others.

If proofs exist, what is the point of all this? Well, to rebuild a
capacity to build a proof, I lost over years. And scholar cases using
the base classes defined so far.
