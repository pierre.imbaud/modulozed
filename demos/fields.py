"""fields: proof z/ p*z is a field
   As of today: tried for half a dozen primes, indeed returns None
   on these.


"""

debug = False

def proof(P):
    """ prooves z/P*z is a field
    P is expected to be prime
    if proof succeeds, an empty list is returned.
    otherwise, the list of non invertible elements is returned.
    """

    global inverses
    inverses = {} 

    for a in range(1, P):
        for b in range(1, P):
             if (a * b) % P == 1:
                 if debug: print ('hit', a, b)
                 inverses[a] = b
                 inverses[b] = a
                 # inverse of a is found, no point continuing:
                 pass
             else:
                 if debug: print ('miss', a, b)
             if a in inverses:
                 pass
    no_inverse = [x for x in range(1, P) if x not in inverses]
    return no_inverse or None
    
