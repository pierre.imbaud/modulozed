#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""provide prime numbers
"""

debug = None

def primes(max):
    """ simplest implementation, yielding primes one at a time.
    already found primes are kept in primes_list.
    We use the sieve of eratosthen.

    the function is an endless loop, and a generator
    """

    primes_list = []

    current = 2

    while max is None or current < max:
        hit = True
        for prime in primes_list:
            if current % prime == 0:
                # current is a multiple of prime: try a next one
                hit = False
                if debug: print('%d to %s failed' % (current, primes_list))
                break
        if hit:
            # tried current against every prime so far: current is a prime!
            primes_list. append(current)
            yield current
        current +=1

                           
