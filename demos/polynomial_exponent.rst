=====================
 Polynomial exponent
=====================

A polynomial can be built on any field, then why not on a finite
field?

The weird thing is that polynomials will be, too, finite sets

Btw, Im not sure finite fields are [*isomorph to*] `ℤ / p*ℤ` (with p
prime); but almost.

On a finite field, x :sup: n , with n > p, will take values already
visited, and hence "cycle" thru the same values. Questions are:

- will the cycle be the same length, whatever the x value?
- if the cycle is p long, should we consider the exponent belongs to
  ℤp, instead of ℕ?

Questions we shall try to answer here.

